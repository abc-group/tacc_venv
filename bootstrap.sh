#!/usr/bin/env bash
set -euo pipefail

# Directories to use
SRC_DIR="$(cd $(dirname "$0") && pwd)"
ENV_DIR="$(pwd)/venv"
NUMPROC=$(nproc)

# Specific commits to checkout
SBLU_COMMIT=65c6348
PRODY_VERSION=1.10.11
PYTORCH_LIGHTNING_VERSION=1.0.0
FAIRSCALE_VERSION="https://github.com/PyTorchLightning/fairscale/archive/pl_1.1.0.zip"

source $SRC_DIR/tacc_modules.txt

# Setup conda env
if [ ! -d "${ENV_DIR}" ]; then
    #conda env create -f "${SRC_DIR}/conda-env.yml" --prefix "${ENV_DIR}"
    conda create -p "${ENV_DIR}" --clone py3_powerai_1.7.0
fi

# Create conda environment in the current directory
set +u  # conda references PS1 variable that is not set in scripts
conda activate "${ENV_DIR}"
set -u

# Setting env variables
set +u
export PKG_CONFIG_PATH="${ENV_DIR}/lib/pkgconfig:${PKG_CONFIG_PATH}"
export PATH="${ENV_DIR}/bin:${ENV_DIR}/lib:${PATH}"
export LD_LIBRARY_PATH="${ENV_DIR}/lib:${LD_LIBRARY_PATH}"
set -u

# Force cmake use gcc
export CC=gcc CXX=g++

# Install DGL
# Release 0.4.3-post2 (https://github.com/dmlc/dgl/releases/tag/0.4.3.post2)
# Using it because later versions need pytorch>=1.5 and TACC has pytorch 1.3 at best.
DGL_COMMIT=5626058
rm -rf dgl
git clone https://github.com/dmlc/dgl.git
cd dgl
git checkout ${DGL_COMMIT}
git submodule update --init --recursive
# Had to tweak CMakeLists
cp $SRC_DIR/deps/CMakeLists.txt .
mkdir build && cd build
cmake -DUSE_CUDA=ON -DCMAKE_INSTALL_PREFIX="${ENV_DIR}" ..
make -j4
make install
cd ../python
python setup.py install
cd ../..

pip install pytorch-lightning==${PYTORCH_LIGHTNING_VERSION}

# for model parallelism
#pip install ${FAIRSCALE_VERSION}

# Install se3 cnn kernels
pip install appdirs # for e3nn
pip install git+https://github.com/AMLab-Amsterdam/lie_learn.git@51b494fc42117575f982ce25977ba5df9682dd3a
pip install git+https://github.com/mariogeiger/se3cnn.git@546bc682887e1cb5e16b484c158c05f03377e4e9

# Install ProDy
pip install pyparsing
pip install biopython
pip install prody==${PRODY_VERSION}

# Install ray tune 
#pip install ray tensorboard hyperopt
#pip install ray[tune]

# Install sb-lab-utils
git clone https://bitbucket.org/bu-structure/sb-lab-utils.git
cd sb-lab-utils
git checkout ${SBLU_COMMIT}
pip install -r requirements/pipeline.txt
python setup.py install
cd ../
rm -rf sb-lab-utils

# Install mol_grid
#pip install git+https://bitbucket.org/ignatovmg/mol_grid.git@5617705c1406c78bc73c7969c376dfe931cdb4ee
