#!/usr/bin/env bash

ROOT="$(cd "$(dirname ${BASH_SOURCE})" && pwd)"
source $ROOT/tacc_modules.txt
conda activate ${ROOT}/venv
